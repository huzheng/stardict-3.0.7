/*
 * Copyright 2023 Hu Zheng <huzheng001@gmail.com>
 *
 * This file is part of StarDict.
 *
 * StarDict is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StarDict is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StarDict.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _STARDICT_VIRTUALDICT2_PLUGIN_H_
#define _STARDICT_VIRTUALDICT2_PLUGIN_H_

struct StarDictVirtualDict2PlugInObject{
	StarDictVirtualDict2PlugInObject();

	typedef void (*multi_lookup_func_t)(const char *text, char ** &dict_name, char **** &ppppWord, char ***** &pppppWordData, bool &bFound);
	multi_lookup_func_t multi_lookup_func;
	const char *total_dict_name;
	const char *author;
	const char *email;
	const char *website;
	const char *date;
};

#endif
