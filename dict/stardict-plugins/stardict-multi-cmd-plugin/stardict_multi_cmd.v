{
	global:
		extern "C" {
			stardict_plugin_init;
			stardict_plugin_exit;
			stardict_virtualdict2_plugin_init;
		};
	local:
		*;
};
